package ru.t1consulting.vmironova.tm.repository;

import ru.t1consulting.vmironova.tm.api.repository.ITaskRepository;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (!task.getProjectId().equals(projectId)) continue;
            if (!userId.equals(task.getUserId())) continue;
            projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

}
