package ru.t1consulting.vmironova.tm.repository;

import ru.t1consulting.vmironova.tm.api.repository.IProjectRepository;
import ru.t1consulting.vmironova.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project(name);
        project.setUserId(userId);
        return add(project);
    }

}
