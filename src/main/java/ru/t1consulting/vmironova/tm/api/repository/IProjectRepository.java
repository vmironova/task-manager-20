package ru.t1consulting.vmironova.tm.api.repository;

import ru.t1consulting.vmironova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
