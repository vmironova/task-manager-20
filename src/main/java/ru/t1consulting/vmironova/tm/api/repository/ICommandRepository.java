package ru.t1consulting.vmironova.tm.api.repository;

import ru.t1consulting.vmironova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArgument(String argument);

    AbstractCommand getCommandByName(String command);

    Collection<AbstractCommand> getTerminalCommands();

}
