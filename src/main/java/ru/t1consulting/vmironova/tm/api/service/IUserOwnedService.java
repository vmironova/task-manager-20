package ru.t1consulting.vmironova.tm.api.service;

import ru.t1consulting.vmironova.tm.api.repository.IUserOwnedRepository;
import ru.t1consulting.vmironova.tm.enumerated.Sort;
import ru.t1consulting.vmironova.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    List<M> findAll(String userId, Sort sort);

}
