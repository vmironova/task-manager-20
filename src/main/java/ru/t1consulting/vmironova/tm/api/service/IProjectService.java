package ru.t1consulting.vmironova.tm.api.service;

import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

}
