package ru.t1consulting.vmironova.tm.command;

import ru.t1consulting.vmironova.tm.api.model.ICommand;
import ru.t1consulting.vmironova.tm.api.service.IAuthService;
import ru.t1consulting.vmironova.tm.api.service.IServiceLocator;
import ru.t1consulting.vmironova.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract String getName();

    public abstract void execute();

    public abstract Role[] getRoles();

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
