package ru.t1consulting.vmironova.tm.command.system;

import ru.t1consulting.vmironova.tm.api.service.ICommandService;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;
import ru.t1consulting.vmironova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    public Role[] getRoles() {
        return null;
    }

}
